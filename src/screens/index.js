import OnBoarding from './onBoarding'
import Home from './home'
import Login from './login'
import SignUp from './signUp'

export { Home, OnBoarding, Login, SignUp }