import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { AntDesign, Entypo } from "@expo/vector-icons";

const Home = () => {
  const [searchPhrase, setSearchPhrase] = useState("");
  const [clicked, setClicked] = useState(false);

  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: "row",
          padding: 10,
          margin: 15,
          borderWidth: 0.5,
          borderRadius: 15,
          alignItems: "center",
        }}
      >
        <View style={{ flex: 0.5, alignItems: "center" }}>
          <AntDesign name="search1" size={20} color="black" />
        </View>

        <TextInput
          style={styles.input}
          placeholder="Search HS code"
          value={searchPhrase}
          onChangeText={(e) => setSearchPhrase(e)}
          onFocus={() => {
            setClicked(true);
          }}
        />

        {searchPhrase && (
          <TouchableOpacity
            style={{ flex: 0.5, alignItems: "center" }}
            onPress={() => setSearchPhrase("")}
          >
            <Entypo name="cross" size={20} color="grey" />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  input: {
    fontSize: 14,
    marginLeft: 10,
    flex: 4,
  },
});
