import { View, Text, Image, ScrollView } from "react-native";
import {
  DrawerItemList,
  createDrawerNavigator,
} from "@react-navigation/drawer";
import { SimpleLineIcons, MaterialIcons, Ionicons } from "@expo/vector-icons";
import { Home } from "../../screens";
import { User } from "../../assets/images";
import { SafeAreaView } from "react-native-safe-area-context";

const Drawer = createDrawerNavigator();

const DrawerNavigation = () => {
  return (
    <Drawer.Navigator
      drawerContent={(props) => {
        return (
          <ScrollView
            contentContainerStyle={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "space-between",
            }}
            showsVerticalScrollIndicator={false}
          >
            <SafeAreaView>
              <View
                style={{
                  height: 200,
                  // width: '100%',
                  justifyContent: "center",
                  alignItems: "center",
                  borderBottomColor: "#f4f4f4",
                  // borderBottomWidth: 1,
                  // marginBottom: 20,
                }}
              >
                <Image
                  source={User}
                  style={{
                    height: 100,
                    width: 100,
                    borderRadius: 50,
                  }}
                />
                <Text
                  style={{
                    fontSize: 16,
                    marginVertical: 6,
                    fontWeight: "bold",
                    color: "#111",
                  }}
                >
                  Guest
                </Text>
                <Text
                  style={{
                    // fontSize: 16,
                    color: "#111",
                  }}
                >
                  Product Manager
                </Text>
              </View>
              <DrawerItemList {...props} />
            </SafeAreaView>
            <View
              style={{
                flexDirection: "row",
                margin: 15,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Text style={{ color: "grey", fontSize: 12 }}>Powered by</Text>
              <Text
                style={{
                  color: "grey",
                  fontSize: 12,
                  marginLeft: 5,
                  fontWeight: "bold",
                }}
              >
                EXIM
              </Text>
            </View>
          </ScrollView>
        );
      }}
      screenOptions={{
        drawerStyle: {
          backgroundColor: "#fff",
          //   width: 250,
        },
        headerStyle: {
          backgroundColor: "#0ea5e9",
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontWeight: "bold",
          fontSize: 14,
        },
        drawerLabelStyle: {
          color: "#111",
        },
        headerTitleAlign: "center",
        drawerType: "slide",
      }}
    >
      <Drawer.Screen
        name="Home"
        options={{
          headerRight: () => (
            <Ionicons
              name="notifications"
              size={24}
              color="#fff"
              style={{ marginRight: 10 }}
            />
          ),
          drawerLabel: "Home",
          title: "Home",
          drawerIcon: () => (
            <SimpleLineIcons name="home" size={20} color="#808080" />
          ),
          // headerShown: false
        }}
        component={Home}
      />
      <Drawer.Screen
        name="Tarif Classification"
        options={{
          drawerLabel: "Tarif Classification",
          title: "Tarif Classification",
          drawerIcon: () => (
            <MaterialIcons name="category" size={20} color="#808080" />
          ),
        }}
        component={Home}
      />
      <Drawer.Screen
        name="Timer"
        options={{
          drawerLabel: "Timer",
          title: "Timer",
          drawerIcon: () => (
            <MaterialIcons name="timer" size={20} color="#808080" />
          ),
        }}
        component={Home}
      />
      <Drawer.Screen
        name="Customize"
        options={{
          drawerLabel: "Customize",
          title: "Customize",
          drawerIcon: () => (
            <MaterialIcons
              name="dashboard-customize"
              size={20}
              color="#808080"
            />
          ),
        }}
        component={Home}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigation;
