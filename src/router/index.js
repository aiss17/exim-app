import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Home, Login, OnBoarding, SignUp } from "../screens";
import Drawer from './drawer'

const Stack = createNativeStackNavigator();

const Router = () => {
    return (
        <Stack.Navigator initialRouteName="onBoarding">
            <Stack.Screen
                name="onBoarding"
                component={OnBoarding}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="drawer"
                component={Drawer}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="login"
                component={Login}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="signUp"
                component={SignUp}
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    )
}

export default Router;