import { OnBoard1, OnBoard2, OnBoard3 } from './onBoarding'
import Background from './background.png'
import Light from './light.png'
import User from './user.jpg'

export { OnBoard1, OnBoard2, OnBoard3, Background, Light, User }